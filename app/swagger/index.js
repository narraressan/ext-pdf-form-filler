const express = require('express');
const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs');
const path = require('path');

const swaggerDoc = YAML.load(path.join(__dirname, 'swagger.yaml'));
swaggerDoc.host = `${process.env.HOST}:${process.env.PORT}`;
swaggerDoc.basePath = `/${process.env.VERSION}`;

const api = express.Router();

api.use('/', swaggerUi.serve);
api.get('/', swaggerUi.setup(swaggerDoc));

module.exports = api;
