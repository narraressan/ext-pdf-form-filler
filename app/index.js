const express = require('express');
const corsPolicy = require('cors');
const bodyParser = require('body-parser');

const health = require('./api/health');
const forms = require('./api/form');
const swagger = require('./swagger');

const app = express();
app.use(corsPolicy());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// attach blueprints
const { VERSION } = process.env;
const { PORT } = process.env;
const { HOST } = process.env;

app.use(`/${VERSION}`, forms);
app.use(`/${VERSION}`, health);
app.use(`/${VERSION}`, swagger);

app.listen(PORT, HOST, () => {
  console.log(`App@: ${HOST}:${PORT}`);
});

module.exports = app;
