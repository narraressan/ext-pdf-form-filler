const fs = require('fs');
const express = require('express');
const moment = require('moment');
const _ = require('lodash');
const {
  ServerException,
  FillupFormFailedException,
} = require('../../utils/exceptions');
const pdfTemplates = require('../../templates');
const filler = require('../../utils/fill_form');

const api = express.Router();

api.get('/templates',
  async (req, res) => {
    try {
      const templates = [];

      _.each(Object.keys(pdfTemplates), (tmp) => {
        const template = {
          name: tmp,
          dir: pdfTemplates[tmp],
          meta: {},
        };

        if (fs.existsSync(template.dir)) {
          const meta = JSON.parse(fs.readFileSync(`${template.dir}/meta.json`));
          template.meta = {
            notes: meta.notes,
            accepts: meta.accepts,
          };
        }

        templates.push(template);
      });

      res.json(templates);
    } catch (err) {
      ServerException.send(res, err);
    }
  });

// NOTE: this should only fill 1 page at a time - to ensure quick response
// ref:
//    - https://stackoverflow.com/a/47311516
//    - https://stackoverflow.com/a/58090515
api.post('/fill',
  async (req, res) => {
    try {
      const { body } = req;
      const template = pdfTemplates[body.template];

      if (fs.existsSync(template)) {
        const output = `${body.template}-${moment().format('MMDDYYhhmmss')}.pdf`;

        res.setHeader('Content-Type', 'application/pdf');
        res.setHeader('Content-disposition', `attachment;filename=${output}`);

        filler.fill(body.template, template, body.data, null, res);

        // use res.download(file); if the file has been saved in local before sending in response
      } else {
        FillupFormFailedException.send(res, 'Filling up form failed.');
      }
    } catch (err) {
      ServerException.send(res, err);
    }
  });

module.exports = api;
