const express = require('express');
const {
  ServerException,
} = require('../../utils/exceptions');

const api = express.Router();

api.get('/health',
  async (req, res) => {
    try {
      res.send('Ok');
    } catch (err) {
      ServerException.send(res, err);
    }
  });

module.exports = api;
