module.exports = {
  apps: [{
    name: 'APP',
    script: './app/index.js',
    instances: 1,
    autorestart: true,
    watch: true,
    ignore_watch: ['./node_modules', './tests', './docker'],
    env: {
      PORT: 3001,
    },
    env_develop: {
      HOST: '0.0.0.0',
      VERSION: 'v1',
    },
    env_prod: {
      HOST: 'https://narraressan.tech/ext-pdf-form-filler/',
      VERSION: '',
    },
  }],
};
