class BaseException {
  static sendError(code = 500, error = 'BASE_EXCEPTION', message = null, response) {
    console.log(`${error}: ${message}`);
    response.status(code).json({ error, message });
  }
}

class ServerException extends BaseException {
  static send(response, message) {
    this.sendError(500, 'SERVER_FAILED', message, response);
  }
}

class FillupFormFailedException extends BaseException {
  static send(response, message) {
    this.sendError(500, 'FILLUP_FORM_FAILED', message, response);
  }
}

module.exports = {
  ServerException,
  FillupFormFailedException,
};
