const fs = require('fs');
const fillPnd1AttachmentPage = require('../templates/pnd1-attachment-page');

const strategies = {
  'PND1-attachment-page': fillPnd1AttachmentPage,
};

const fill = (strategy, dir, toFill, outputName, pipe = null) => {
  // read the meta file
  const metaData = fs.readFileSync(`${dir}/meta.json`);
  const meta = JSON.parse(metaData);

  return strategies[strategy](meta, dir, toFill, outputName, pipe);
};

module.exports = {
  fill,
};
