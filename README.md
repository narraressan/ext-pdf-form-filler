# ext-pdf-form-filler

This project aims to allow clients to automate filling up of PDF forms by simply sending data to the API and expect a PDF filled with the data provided.

As this is a very young project, we only have a limited number of PDF templates supported. You are welcome and encouraged to add any custom PDFs on this project.

Check the swagger for the list of endpoints which includes viewing the list of supported templates.


### Setup local dev
``` bash
# install dependencies
npm install --verbose

# start the application at localhost:3001
# note that the application is served with pm2 ecosystem.
npm start
npm run logs

# stop and deletes the service/s running in pm2
npm run stop

# to run tests, simply execute
npm run test
npm run unit-test
npm run integration-test

# run and fix linter manually. 
# this should also be triggered during pre-commit
npm run lint

# run dev via docker-compose - highly recommended!
docker-compose --file ./docker/dev/dev.yml up -d --build

# view logs
docker-compose --file ./docker/dev/dev.yml logs -f --tail 10
```
 
Read more about pm2 ecosystem [here](https://pm2.keymetrics.io/docs/usage/application-declaration/) as it is the core process manager of this application

Also, the core PDF library is PDFkit which is in active development. Read more about it [here](https://pdfkit.org/)

Lastly, the main framework used to serving the RESTful service is Express Js. Find out more about it [here](https://expressjs.com/)


### Build
``` bash
# docker for prod
docker-compose --file ./docker/prod/prod.yml build
```


### Publish docker
``` bash
# login using specific user docker login docker.io -u username -p password
docker login docker.io

# publish the image to your docker repository
docker tag prod_ext-pdf-form-filler [username]/ext-pdf-form-filler:latest
docker push [username]/ext-pdf-form-filler:latest

# logout from docker
docker logout
```

Note:

Check the file `./docker/prod/test.yml` for reference on how to call the image.


### Future releases
- support more PDF templates
- allow users to upload their own templates
- add an interactive UI for mapping the coordinates of the form field


> more to come soon probably a full blown service derived from this simple project ...
