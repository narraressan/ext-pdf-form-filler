/* eslint-disable */
const supertest = require('supertest');
const app = require('../app/index.js');
const data = require('./sample-request');

const version = 'v1';

// start and clean dependencies
beforeAll((done) => {
  server = app.listen(done);
  request = supertest.agent(server);
});
afterAll((done) => {
  server.close(done);
});

describe('Ping health endpoint', () => {
  it('Should receive a valid response', async () => {
    const res = await request.get(`/${version}/health`).send();
    expect(res.statusCode).toEqual(200);
  });
});

describe('Test fill form endpoint', () => {
  it('Should receive a file response', async () => {
    const res = await request
                  .post(`/${version}/fill`)
                  .send(data);
    
    expect(res.statusCode).toEqual(200);
  });
});
