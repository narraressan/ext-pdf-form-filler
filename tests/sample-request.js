module.exports = {
  template: 'PND1-attachment-page',
  data: {
    // this is from the "accepts" response of `api/templates`
    cover: {
      month: [true, true, true, true, true, true, true, true, true, true, true, true],
      year: '2569',
      tax_no: 'X-XXXX-XXXXX-XX-X',
      branch_no: 'XXXXX',
      office_name: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
      attach_pages: '0000',
      count: '0000',
      total_taxable_income: '100,345.09',
      total_tax_deductions: '1,234,340.00',
    },
    pages: [
      {
        tax_no: '5-4327-23896-00-7',
        branch_no: '56901',
        page_of: '1',
        pages: '100',
        income_type: [true, true, false, true, false],
        employees: [
          {
            sequence_no: '1',
            employee_id: '5-3467-12326-11-1',
            employee_prefix_nd_firstname: 'Mr Ghdyesf',
            employee_lastname: 'Lkdj Hl',
            paid_date: '03/30/2020',
            taxable_income: '25,000.00',
            tax_deductions: '3500',
            tax_condition: '1',
          }, {
            sequence_no: '2',
            employee_id: 'X-2222-22222-22-2',
            employee_prefix_nd_firstname: 'Ms. Jane',
            employee_lastname: 'Doe',
            paid_date: '03/30/2020',
            taxable_income: '25,000.00',
            tax_deductions: '3,500.00',
            tax_condition: '1',
          }, {
            sequence_no: '3',
            employee_id: '3-3333-33333-33-3',
            employee_prefix_nd_firstname: 'Mr. Adean',
            employee_lastname: 'Ladia',
            paid_date: '03/30/2020',
            taxable_income: '25,000.00',
            tax_deductions: '3,500.00',
            tax_condition: '1',
          }, {
            sequence_no: '4',
            employee_id: '4-4444-44444-44-4',
            employee_prefix_nd_firstname: 'Ms. New',
            employee_lastname: 'Men',
            paid_date: '03/30/2020',
            taxable_income: '25,000.00',
            tax_deductions: '3,500.00',
            tax_condition: '1',
          }, {
            sequence_no: '5',
            employee_id: '5-9677-55555-33-5',
            employee_prefix_nd_firstname: 'Mr. Nasser',
            employee_lastname: 'Bacaca',
            paid_date: '03/30/2020',
            taxable_income: '25,000.00',
            tax_deductions: '3,500.00',
            tax_condition: '1',
          }, {
            sequence_no: '6',
            employee_id: '6-6666-66666-66-6',
            employee_prefix_nd_firstname: 'Mr. Jimmy',
            employee_lastname: 'Rodriguez',
            paid_date: '03/30/2020',
            taxable_income: '25,000.00',
            tax_deductions: '3,500.00',
            tax_condition: '1',
          }, {
            sequence_no: '7',
            employee_id: '7-7777-34347-77-7',
            employee_prefix_nd_firstname: 'Mr. Naphy',
            employee_lastname: 'Lozadas',
            paid_date: '03/30/2020',
            taxable_income: '25,000.00',
            tax_deductions: '3,500.00',
            tax_condition: '1',
          }, {
            sequence_no: '8',
            employee_id: '8-8934-88888-88-8',
            employee_prefix_nd_firstname: 'Ms. Katherine',
            employee_lastname: 'De la cruz',
            paid_date: '03/30/2020',
            taxable_income: '25,000.00',
            tax_deductions: '3,500.00',
            tax_condition: '1',
          },
        ],
        total_taxable_income: '200,000.00',
        total_tax_deductions: '8,000.00',
      }, {
        tax_no: '5-4327-23896-00-7',
        branch_no: '5690',
        page_of: '2',
        pages: '100',
        income_type: [true, true, false, true, false],
        employees: [
          {
            sequence_no: '9',
            employee_id: '0-0000-00000-00-0',
            employee_prefix_nd_firstname: 'Ms. Salome',
            employee_lastname: 'Mendoza',
            paid_date: '03/30/2020',
            taxable_income: '25,000.00',
            tax_deductions: '3000',
            tax_condition: '1',
          }, {
            sequence_no: '10',
            employee_id: '2-2222-22222-22-2',
            employee_prefix_nd_firstname: 'Ms. Jane',
            employee_lastname: 'Doe',
            paid_date: '03/30/2020',
            taxable_income: '25,000.00',
            tax_deductions: '3000',
            tax_condition: '1',
          }, {
            sequence_no: '11',
            employee_id: '3-HH73-567FF-33-3',
            employee_prefix_nd_firstname: 'Mr. Testing',
            employee_lastname: 'Name Jr.',
            paid_date: '03/30/2020',
            taxable_income: '25,000.00',
            tax_deductions: '3000',
            tax_condition: '1',
          }, {
            sequence_no: '12',
            employee_id: '6-JH89-JH896-66-6',
            employee_prefix_nd_firstname: 'Mr. Harambae',
            employee_lastname: 'Jungly',
            paid_date: '03/30/2020',
            taxable_income: '25,000.00',
            tax_deductions: '3000',
            tax_condition: '1',
          }, {
            sequence_no: '13',
            employee_id: '5-5555-55555-55-5',
            employee_prefix_nd_firstname: 'Ms. Kate',
            employee_lastname: 'Cabredilla',
            paid_date: '03/30/2020',
            taxable_income: '25,000.00',
            tax_deductions: '3000',
            tax_condition: '1',
          }, {
            sequence_no: '14',
            employee_id: '6-JH89-JH896-66-6',
            employee_prefix_nd_firstname: 'Ms Gemini',
            employee_lastname: 'Zodiac',
            paid_date: '03/30/2020',
            taxable_income: '25,000.00',
            tax_deductions: '3000',
            tax_condition: '1',
          }, {
            sequence_no: '15',
            employee_id: '7-7777-X892J-77-7',
            employee_prefix_nd_firstname: 'Mr. Lance',
            employee_lastname: 'Trinity',
            paid_date: '03/30/2020',
            taxable_income: '25,000.00',
            tax_deductions: '3000',
            tax_condition: '1',
          }, {
            sequence_no: '16',
            employee_id: '8-86FT-YYU73-88-8-99',
            employee_prefix_nd_firstname: 'Mr. Dante',
            employee_lastname: 'Ortho',
            paid_date: '03/30/2020',
            taxable_income: '25,000.00',
            tax_deductions: '3000',
            tax_condition: '1',
          },
        ],
        total_taxable_income: '200,000.00',
        total_tax_deductions: '8,000.00',
      },
    ],
  },
};
