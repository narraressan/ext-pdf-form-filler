/* eslint-disable */
const request = require('./sample-request');

describe('Test fill form functions', () => {
  test('Should create a file after finished', () => {
    const fs = require('fs');
    const path = require('path');
    const templates = require('../templates');
    const filler = require('../utils/fill_form');
    const moment = require('moment');
    
    const source = path.join(__dirname, `../${templates[request.template]}`);
    const output = `./tests/${request.template}-${moment().format('MMDDYYhhmmss')}-test.pdf`;
    
    filler.fill(request.template, source, request.data, output);
    expect(fs.existsSync(output)).toEqual(true);
  })
})