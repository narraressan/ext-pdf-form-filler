// create a document and pipe to a blob
// https://www.papersizes.org/a-sizes-in-pixels.htm
var doc = new PDFDocument({
        "layout": "portrait", 
        "page-size": "A4"
    });
doc.fontSize(10)

var stream = doc.pipe(blobStream());

var check = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAABmJLR0QA/wD/AP+gvaeTAAAAbUlEQVRIie2SMQqAMAxF3yUUvf9JnERaFzt4HB3aocQWB1MokgdZQnh8wgfD6IkR2IGllTwAF7BpywfgSPITmEzev3wFPLEZkrwtoXLziqsIVOTwfMFc2H3+uUyrklySp27SFohpXRq15MbPuAEcIC00f9uqxwAAAABJRU5ErkJggg=='
var checkSize = { width: 12, height: 12 }

var xhr = new XMLHttpRequest();
xhr.responseType = 'arraybuffer';
var url = './cover1.jpg'
xhr.open('GET', url, true);

var xhr2 = new XMLHttpRequest();
xhr2.responseType = 'arraybuffer';
var url2 = './cover2.jpg'
xhr2.open('GET', url2, true);

xhr.onload = function() {
    // set the font using the arraybuffer returned from the xhr
    doc.image(xhr.response, 0, 20, { width: 595, height: 780 });
    
    // checkbox area
    doc.image(check, 445, 125, checkSize);
    doc.image(check, 338, 148, checkSize);
    doc.image(check, 338, 170, checkSize);
    doc.image(check, 338, 193, checkSize);
    doc.image(check, 395, 148, checkSize);
    doc.image(check, 395, 170, checkSize);
    doc.image(check, 395, 193, checkSize);
    doc.image(check, 453, 148, checkSize);
    doc.image(check, 453, 170, checkSize);
    doc.image(check, 453, 193, checkSize);
    doc.image(check, 507, 148, checkSize);
    doc.image(check, 507, 170, checkSize);
    doc.image(check, 507, 193, checkSize);
    doc.text(`XXXXXX`, 520, 125, { width: 45 });
    
    // tax id number
    doc.text(`X`, 163, 110, { characterSpacing: 5 });
    doc.text(`XXXX`, 179, 110, { characterSpacing: 5 });
    doc.text(`XXXXX`, 230, 110, { characterSpacing: 5 });
    doc.text(`XX`, 293, 110, { characterSpacing: 5 });
    doc.text(`X`, 321, 110, { characterSpacing: 5 });
    
    // company tax branch code
    doc.text(`XXXXX`, 277, 138, { characterSpacing: 5 });
    
    // office name
    doc.text(`XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX`, 50, 150, { width: 270 });
    
    // record type
    doc.image(check, 83, 264, checkSize);
    doc.text(`x`, 286, 264);
    
    // amount/number of attachment page
    doc.text(`XXXX`, 505, 307, { width: 30 });
    
    // amount/number of employees
    doc.text(`0,000,000`, 319, 405, { width: 50, align: 'center' });
    doc.text(`0,000,000`, 319, 535, { width: 50, align: 'center' });
    
    // total taxable income
    doc.text(`0,000,000,000.00`, 370, 405, { width: 85, align: 'right' });
    doc.text(`0,000,000,000.00`, 370, 535, { width: 85, align: 'right' });
    
    // total tax deduction
    doc.text(`0,000,000,000.00`, 455, 405, { width: 85, align: 'right' });
    doc.text(`0,000,000,000.00`, 455, 535, { width: 85, align: 'right' });
    
    // total tax
    doc.text(`0,000,000,000.00`, 455, 568, { width: 85, align: 'right' });
    
    xhr2.send();
};
xhr.send();


xhr2.onload = function() {
    doc.addPage({
        "layout": "portrait", 
        "page-size": "A4"
    })
    
    // set the font using the arraybuffer returned from the xhr
    doc.image(xhr2.response, 0, 0, { width: 595, height: 800 });
    doc.fontSize(10)
    
    

    // end and display the document in the iframe to the right
    doc.end();
    stream.on('finish', function() {
      iframe.src = stream.toBlobURL('application/pdf');
    });
};


