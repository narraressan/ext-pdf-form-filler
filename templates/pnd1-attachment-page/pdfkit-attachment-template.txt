// create a document and pipe to a blob
var doc = new PDFDocument({
        "layout": "portrait", 
        "size": [841, 595]
    });
var stream = doc.pipe(blobStream());

var xhr = new XMLHttpRequest;
xhr.onload = function() {
    // set the font using the arraybuffer returned from the xhr
    doc.image(xhr.response, 0, 0, { width: 841, height: 595 });
    doc.fontSize(10)
    
    // company tax no
    doc.text(`X`, 445, 22);
    doc.text(`67390`, 463, 22, { characterSpacing: 5 });
    doc.text(`67390X`, 517, 22, { characterSpacing: 5 });
    doc.text(`XX`, 582, 22, { characterSpacing: 5 });
    doc.text(`X`, 612, 22);
    
    
    // branch no
    doc.text(`67390X`, 752, 50, { width: 80, characterSpacing: 4 });


    // page of + total
    doc.text(`XXX`, 692, 107, { width: 30 });
    doc.text(`XXX`, 759, 107, { width: 30 });
    
    
    // income type
    var check = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAABmJLR0QA/wD/AP+gvaeTAAAAbUlEQVRIie2SMQqAMAxF3yUUvf9JnERaFzt4HB3aocQWB1MokgdZQnh8wgfD6IkR2IGllTwAF7BpywfgSPITmEzev3wFPLEZkrwtoXLziqsIVOTwfMFc2H3+uUyrklySp27SFohpXRq15MbPuAEcIC00f9uqxwAAAABJRU5ErkJggg=='
    var checkSize = { width: 15, height: 15 }
    // doc.image(check, 377, 48, checkSize);
    doc.image(check, 117, 66, checkSize);
    doc.image(check, 117, 82, checkSize);
    doc.image(check, 349, 66, checkSize);
    doc.image(check, 349, 82, checkSize);
    doc.image(check, 349, 100, checkSize);
    
    
    // employee 1 -------------------------
    
    // sequence no
    doc.text(`67390`, 68, 170);
    
    // employee id
    doc.text(`X`, 108, 170);
    doc.text(`67390`, 126, 170, { characterSpacing: 5 });
    doc.text(`67390X`, 180, 170, { characterSpacing: 5 });
    doc.text(`XX`, 245, 170, { characterSpacing: 5 });
    doc.text(`X`, 275, 170);
    
    // employee name (prefix + firstname)
    doc.text(`6739067390673906739067390XXX`, 118, 187);
    
    // employee name (lastname)
    doc.text(`6739067390673906739067390XXX`, 307, 187);
    
    // paid date
    doc.text(`673906739067390XX`, 477, 187);
    
    // taxable income
    doc.text(`6739067390XXX`, 587, 187);
    
    // deductions
    doc.text(`6739067390XXX`, 690, 187);
    
    // tax condition
    doc.text(`XX`, 789, 187);
    
    // ---------------------------------------
    
    
    // employee 2 -------------------------
    
    // sequence no
    doc.text(`67390`, 68, 208);
    
    // employee id
    doc.text(`X`, 108, 208);
    doc.text(`67390`, 126, 208, { characterSpacing: 5 });
    doc.text(`67390X`, 180, 208, { characterSpacing: 5 });
    doc.text(`XX`, 245, 208, { characterSpacing: 5 });
    doc.text(`X`, 275, 208);
    
    // employee name (prefix + firstname)
    doc.text(`6739067390673906739067390XXX`, 118, 224);
    
    // employee name (lastname)
    doc.text(`6739067390673906739067390XXX`, 307, 224);
    
    // paid date
    doc.text(`673906739067390XX`, 477, 224);
    
    // taxable income
    doc.text(`6739067390XXX`, 587, 224);
    
    // deductions
    doc.text(`6739067390XXX`, 690, 224);
    
    // tax condition
    doc.text(`XX`, 789, 224);
    
    // ---------------------------------------
    
    
    // employee 3 -------------------------
    
    // sequence no
    doc.text(`67390`, 68, 246);
    
    // employee id
    doc.text(`X`, 108, 246);
    doc.text(`67390`, 126, 246, { characterSpacing: 5 });
    doc.text(`67390X`, 180, 246, { characterSpacing: 5 });
    doc.text(`XX`, 245, 246, { characterSpacing: 5 });
    doc.text(`X`, 275, 246);
    
    // employee name (prefix + firstname)
    doc.text(`6739067390673906739067390XXX`, 118, 262);
    
    // employee name (lastname)
    doc.text(`6739067390673906739067390XXX`, 307, 262);
    
    // paid date
    doc.text(`673906739067390XX`, 477, 262);
    
    // taxable income
    doc.text(`6739067390XXX`, 587, 262);
    
    // deductions
    doc.text(`6739067390XXX`, 690, 262);
    
    // tax condition
    doc.text(`XX`, 789, 262);
    
    // ---------------------------------------
    
    
    // employee 4 -------------------------
    
    // sequence no
    doc.text(`67390`, 68, 286);
    
    // employee id
    doc.text(`X`, 108, 286);
    doc.text(`67390`, 126, 286, { characterSpacing: 5 });
    doc.text(`67390X`, 180, 286, { characterSpacing: 5 });
    doc.text(`XX`, 245, 286, { characterSpacing: 5 });
    doc.text(`X`, 275, 286);
    
    // employee name (prefix + firstname)
    doc.text(`6739067390673906739067390XXX`, 118, 302);
    
    // employee name (lastname)
    doc.text(`6739067390673906739067390XXX`, 307, 302);
    
    // paid date
    doc.text(`673906739067390XX`, 477, 302);
    
    // taxable income
    doc.text(`6739067390XXX`, 587, 302);
    
    // deductions
    doc.text(`6739067390XXX`, 690, 302);
    
    // tax condition
    doc.text(`XX`, 789, 302);
    
    // ---------------------------------------
    
    
    // employee 5 -------------------------
    
    // sequence no
    doc.text(`67390`, 68, 323);
    
    // employee id
    doc.text(`X`, 108, 323);
    doc.text(`67390`, 126, 323, { characterSpacing: 5 });
    doc.text(`67390X`, 180, 323, { characterSpacing: 5 });
    doc.text(`XX`, 245, 323, { characterSpacing: 5 });
    doc.text(`X`, 275, 323);
    
    // employee name (prefix + firstname)
    doc.text(`6739067390673906739067390XXX`, 118, 340);
    
    // employee name (lastname)
    doc.text(`6739067390673906739067390XXX`, 307, 340);
    
    // paid date
    doc.text(`673906739067390XX`, 477, 340);
    
    // taxable income
    doc.text(`6739067390XXX`, 587, 340);
    
    // deductions
    doc.text(`6739067390XXX`, 690, 340);
    
    // tax condition
    doc.text(`XX`, 789, 340);
    
    // ---------------------------------------
    
    
    // employee 6 -------------------------
    
    // sequence no
    doc.text(`3`, 68, 360, { width: 30, align: 'center' });
    
    // employee id
    doc.text(`X`, 108, 360);
    doc.text(`67390`, 126, 360, { characterSpacing: 5 });
    doc.text(`67390X`, 180, 360, { characterSpacing: 5 });
    doc.text(`XX`, 245, 360, { characterSpacing: 5 });
    doc.text(`X`, 275, 360);
    
    // employee name (prefix + firstname)
    doc.text(`6739067390673906739067390XXX`, 118, 376);
    
    // employee name (lastname)
    doc.text(`6739067390673906739067390XXX`, 307, 376);
    
    // paid date
    doc.text(`673906739067390XX`, 477, 376);
    
    // taxable income
    doc.text(`6739067390XXX`, 587, 376);
    
    // deductions
    doc.text(`6739067390XXX`, 690, 376);
    
    // tax condition
    doc.text(`XX`, 789, 376);
    
    // ---------------------------------------
    
    
    // employee 7 -------------------------
    
    // sequence no
    doc.text(`67390`, 68, 398);
    
    // employee id
    doc.text(`X`, 108, 398);
    doc.text(`67390`, 126, 398, { characterSpacing: 5 });
    doc.text(`67390X`, 180, 398, { characterSpacing: 5 });
    doc.text(`XX`, 245, 398, { characterSpacing: 5 });
    doc.text(`X`, 275, 398);
    
    // employee name (prefix + firstname)
    doc.text(`6739067390673906739067390XXX`, 118, 414);
    
    // employee name (lastname)
    doc.text(`6739067390673906739067390XXX`, 307, 414);
    
    // paid date
    doc.text(`2020/03/20`, 477, 414, { "width": 95, "align": "center" });
    
    // taxable income
    doc.text(`6739067390XX`, 587, 414, { "width": 75, "align": "center" });
    
    // taxable deductions
    doc.text(`6739067390XX`, 690, 414, { "width": 75, "align": "center" });
    
    // tax condition
    doc.text(`XX`, 789, 414, { "width": 15, "align": "center"});
    
    // ---------------------------------------
    
    
    // employee 8 -------------------------
    
    // sequence no
    doc.text(`67390`, 68, 436);
    
    // employee id
    doc.text(`X`, 108, 436);
    doc.text(`67390`, 126, 436, { characterSpacing: 5 });
    doc.text(`67390X`, 180, 436, { characterSpacing: 5 });
    doc.text(`XX`, 245, 436, { characterSpacing: 5 });
    doc.text(`X`, 275, 436);
    
    // employee name (prefix + firstname)
    doc.text(`6739067390675678739067388234`, 118, 452);
    
    // employee name (lastname)
    doc.text(`6732367368673911239067390532`, 307, 452);
    
    // paid date
    doc.text(`673906739067390XX`, 477, 452);
    
    // taxable income
    doc.text(`8839022392334`, 587, 452);
    
    // deductions
    doc.text(`6558967390234`, 690, 452);
    
    // tax condition
    doc.text(`XX`, 789, 452);
    
    // ---------------------------------------
    
    // total taxable income
    doc.text(`000,000,000.00`, 587, 473, { "width": 75, "align": "center" });
    
    // total taxable deductions
    doc.text(`000,000,000.00`, 690, 473, { "width": 75, "align": "center" });

    // end and display the document in the iframe to the right
    doc.end();
    stream.on('finish', function() {
      iframe.src = stream.toBlobURL('application/pdf');
    });
};

xhr.responseType = 'arraybuffer';
var url = './bg.jpg'
xhr.open('GET', url, true);
xhr.send();