const PDFDocument = require('pdfkit');
const fs = require('fs');
const _ = require('lodash');

const addBG = (doc, dir, opts, x = 0, y = 0) => {
  doc.image(dir, x, y, { width: opts.size[0], height: opts.size[1] });
};

const addText = (doc, val, opts, family = 'fonts/Helvetica.ttf') => {
  // consider adding width and elispes if text exceeds
  // ellipses may require both width and height
  doc.font(family).text(val, opts.x, opts.y, opts.opts);
};

const addCheck = (doc, val, coords) => {
  const check = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAABmJLR0QA/wD/AP+gvaeTAAAAbUlEQVRIie2SMQqAMAxF3yUUvf9JnERaFzt4HB3aocQWB1MokgdZQnh8wgfD6IkR2IGllTwAF7BpywfgSPITmEzev3wFPLEZkrwtoXLziqsIVOTwfMFc2H3+uUyrklySp27SFohpXRq15MbPuAEcIC00f9uqxwAAAABJRU5ErkJggg==';
  const size = { width: 15, height: 15 };

  if (val) {
    doc.image(check, coords.x, coords.y, size);
  }
};

const addFormatedId = (doc, val, opts) => {
  const ids = val.split('-');
  _.each(ids, (id, i) => {
    if (opts[i]) {
      addText(doc, id, opts[i]);
    }
  });
};

const addEmployee = (doc, val, opts) => {
  // sequence no
  addText(doc, val.sequence_no, opts.sequence_no);

  // employee id
  addFormatedId(doc, val.employee_id, opts.employee_id);

  // employee name (prefix + firstname)
  addText(doc, val.employee_prefix_nd_firstname, opts.employee_prefix_nd_firstname, 'fonts/Trirong-SemiBold.ttf');

  // employee name (lastname)
  addText(doc, val.employee_lastname, opts.employee_lastname, 'fonts/Trirong-SemiBold.ttf');

  // paid date
  addText(doc, val.paid_date, opts.paid_date);

  // taxable income
  addText(doc, val.taxable_income, opts.taxable_income);

  // deductions
  addText(doc, val.tax_deductions, opts.tax_deductions);

  // tax condition
  addText(doc, val.tax_condition, opts.tax_condition);
};

const fillPage = (doc, page, opts) => {
  // company tax no
  addFormatedId(doc, page.tax_no, opts.fields.tax_no);

  // branch no
  addText(doc, page.branch_no, opts.fields.branch_no);

  // current page
  addText(doc, page.page_of, opts.fields.page_of);

  // total pages
  addText(doc, page.pages, opts.fields.pages);

  // income type
  _.each(page.income_type, (check, j) => {
    addCheck(doc, check, opts.fields.income_type[j]);
  });

  // employees
  _.each(page.employees, (employee, k) => {
    if (opts.fields.employees[k]) {
      addEmployee(doc, employee, opts.fields.employees[k]);
    }
  });

  // total taxable income
  addText(doc, page.total_taxable_income, opts.fields.total_taxable_income);

  // total taxable deductions
  addText(doc, page.total_tax_deductions, opts.fields.total_tax_deductions);
};

const fillCover = (doc, cover, opts, dir) => {
  const coverFields = opts.fields.cover;

  // bg page 1
  addBG(doc, `${dir}/cover1.jpg`, { size: [595, 780] }, 0, 20);

  // checkbox area
  _.each(cover.month, (check, i) => {
    addCheck(doc, check, coverFields.month[i]);
  });
  addText(doc, cover.year, coverFields.year);

  // tax id number
  addFormatedId(doc, cover.tax_no, coverFields.tax_no);

  // company tax branch code
  addText(doc, cover.branch_no, coverFields.branch_no);

  // office name
  addText(doc, cover.office_name, coverFields.office_name, 'fonts/Trirong-SemiBold.ttf');

  // record type
  addCheck(doc, true, { x: 83, y: 261 });
  addText(doc, 'x', { x: 286, y: 264 });
  addCheck(doc, true, { x: 263, y: 303 });

  // amount/number of attachment page
  addText(doc, cover.attach_pages, coverFields.attach_pages);

  // amount/number of employees
  addText(doc, cover.count, coverFields.count);

  // total taxable income
  addText(doc, cover.total_taxable_income, coverFields.total_taxable_income);

  // total tax deduction
  addText(doc, cover.total_tax_deductions, coverFields.total_tax_deductions);

  // total tax - default position
  addText(doc, cover.count, { x: 319, y: 535, opts: { width: 50, align: 'center' } });
  addText(doc, cover.total_taxable_income, { x: 369, y: 535, opts: { width: 85, align: 'right' } });
  addText(doc, cover.total_tax_deductions, { x: 455, y: 535, opts: { width: 85, align: 'right' } });
  addText(doc, cover.total_tax_deductions, { x: 455, y: 568, opts: { width: 85, align: 'right' } });
  // ---------------------------------------------------------------------------

  // bg page 2
  doc.addPage(opts.cover);
  addBG(doc, `${dir}/cover2.jpg`, { size: [595, 780] }, 0, 20);
  // ---------------------------------------------------------------------------
};

module.exports = (meta, dir, toFill, outputName, pipe = null) => {
  // prepare the pdfkit
  const doc = new PDFDocument(meta.cover);
  doc.fontSize(10);

  fillCover(doc, toFill.cover, meta, dir);

  // attachment pages
  doc.addPage(meta.document);
  addBG(doc, `${dir}/bg.jpg`, meta.document);

  _.each(toFill.pages, (page, i) => {
    fillPage(doc, page, meta);

    if (i < (toFill.pages.length - 1)) {
      doc.addPage(meta.document);
      addBG(doc, `${dir}/bg.jpg`, meta.document);
      console.log(`continue in page: ${i}`);
    }
  });

  // you may pip direclty to the amazone s3
  // hef: ttps://stackoverflow.com/a/35663466
  // or directly send as response in express.js
  // ref: https://github.com/foliojs/pdfkit/issues/633#issuecomment-286186962
  // https://stackoverflow.com/a/23624716
  if (pipe != null) {
    doc.pipe(pipe);
  } else {
    doc.pipe(fs.createWriteStream(outputName));
  }

  doc.end();
};
